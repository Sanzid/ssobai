<?php

require_once 'config.php';

if(!isset($_SESSION['id'], $_SESSION['email'])){
	$_SESSION['message'] = 'You need to login to access this page.' ;
	header('Location: login.php');
	exit();
}

$message = $_SESSION['message']?? null; 

require_once 'layouts/header.php';
?>

		<div class="alert alert-info">
			You Have been Logged In as, <?php echo $_SESSION['email']; ?>
		</div>

		<div>
			
			<form action="update_password.php" method="post" accept-charset="utf-8">
				<?php require_once 'layouts/notification.php'; ?>
				<div class="form-group">
					<label for="current_password">Current Password </label>
						<input type="password" name="current_password" id="current_password" class="form-control" required>
				</div>

				<div class="form-group">
					<label for="new_password">New Password </label>
						<input type="password" name="new_password" id="new_password" class="form-control" required>
				</div>

				<div class="form-group">
					<label for="confirm_new_password">Confirm New Password </label>
						<input type="password" name="confirm_new_password" id="confirm_new_password" class="form-control" required>
				</div>

				<button type="submit" name="change" class="btn btn-success">Change Password</button>
			</form>
		</div>
		<p></p>
		<a href="logout.php" class="btn btn-danger">Log out</a>

	<?php require_once 'layouts/footer.php';