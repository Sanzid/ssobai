<?php
require_once 'config.php';


if(isset($_POST['register'])){
	$email= strtolower(trim($_POST['email']));
	$username= strtolower(trim($_POST['username']));
	$password= trim($_POST['password']);
	$password = password_hash($password, PASSWORD_BCRYPT);
	$activation_token= sha1(uniqid($username.$email.time(), true));

	require_once 'helpers/connection.php';

	$query ='INSERT INTO users (email,username,password,activation_token, created_at) VALUES (:email, :username, :password, :activation_token, :created_at)';

	$stmt= $connection -> prepare($query);

	$stmt->bindParam(':email', $email);
	$stmt->bindParam(':username', $username);
	$stmt->bindParam(':password', $password);
	$stmt->bindParam(':activation_token', $activation_token);
	$stmt->bindValue(':created_at', date('Y-m-d H:i:s'));

	$response = $stmt->execute();

	if($response === true){
		$mail = new \PHPMailer\PHPMailer\PHPMailer();
		try{
			//Server settings
	    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
	    $mail->isSMTP();                                      // Set mailer to use SMTP
	    $mail->Host = 'smtp.mailtrap.io';  						// Specify main and backup SMTP servers
	    $mail->SMTPAuth = true;                               // Enable SMTP authentication
	    $mail->Username = 'a725effc96ed6a';                 // SMTP username
	    $mail->Password = '9f4de1af7a8b3a';                           // SMTP password
	    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->Port = 465;   

	    //Recipients
	    $mail->setFrom('hello@dhaka.com', 'Sanzid');
	    $mail->addAddress($email, $username);     // Add a recipient        

	    //Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = 'Account Created as, ' .$username ;
	    $mail->Body    = 'Dear, '. $email .'<br/>';
	    $mail->Body    .= 'Your Account created Successfully! <br/>';
	    $mail->Body    .= 'Please Click this link to activate your account: <br/>';
	    $mail->Body    .= '<a href="http://localhost/ssb98/crud/activate.php?token='.$activation_token.'">
							http://localhost/ssb98/crud/activate.php?token='.$activation_token.'</a>';
	    $mail->Body    .= '<br/>';
	    $mail->send();

	}catch(Execption $e){

	$_SESSION['message']= $e->getMessage();

}

//set message & type
notification('User Created Successfully.');
redirect();
}

notification('Something Went Wrong! Please try again.');
redirect();
}

?>
