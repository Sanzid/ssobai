<?php

require_once 'config.php';

if(!is_logged_in()){

	notification('You need to login to access this page. ', 'danger');
	redirect('login.php');
}

if(!is_admin()){

	notification('You need to be an admin to access this page. ', 'danger');
	redirect('login.php');
}

$message = $_SESSION['message']?? null;

// $query = 'SELECT count(id) FROM users WHERE id=:id';
// $stmt = $connection->prepare($query);
// $stmt->bindParam(':id', $user_id, PDO::PARAM_INT);
// $stmt->execute();

$id=(int)$_GET['id'];
$query='SELECT count(id) as is_available, email, username, address, active, role FROM users WHERE id=:id';
$stmt = $connection->prepare($query);
$stmt -> bindParam(':id', $id, PDO::PARAM_INT);
$stmt -> execute();

$user = $stmt->fetch();

if((int)$user['is_available']=== 0){
	notification('User with id '.$id. ' does not exist', 'danger');
	redirect('users.php');
}


require_once 'layouts/header.php';
?>

	
	<form action="update_user.php" method="post">
			<input type="hidden" name="user_id" value="<?php echo $id; ?>">
			<?php require_once 'layouts/notification.php'; ?>

			<div class="form-group">
				<label for="exampleInputEmail1">Email address</label>
				<input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $user['email']; ?>" required>
				<small id="emailHelp" class="form-text text-muted">We will never share your email with anyone else.</small>
			</div>
			<div class="form-group">
				<label for="exampleInputUsername">User Name</label>
				<input type="text" class="form-control" name="username" id="exampleInputUsername" value="<?php echo $user['username']; ?>" required>
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">Password (leave blank for no update)</label>
				<input type="password" class="form-control" name="password" id="exampleInputPassword1">
			</div>
			<div class="form-group">
				<label for="exampleInputAddress">Address</label>
				<textarea class="form-control" name="address" id="exampleInputAddress" required>
					<?php echo $user['address']; ?>
				</textarea>
			</div>
			<div class="form-group">
				<label for="exampleInputActive">Active</label>
				<input type="radio" name="active" value="1" <?php if((int)$user['active']=== 1): echo 'checked'; endif; ?> id="exampleInputActive" class="form-control">Yes
				<input type="radio" name="active" value="0" <?php if((int)$user['active']=== 0): echo 'checked'; endif; ?> id="exampleInputActive" class="form-control">No
			</div>
			<div class="form-group">
				<label for="exampleInputRole">Role</label>
				<select name="role" id="exampleInputRole" class="form-control">
					<option value="user" <?php if($user['role']==='user'): echo 'selected'; endif; ?> >User</option>
					<option value="admin" <?php if($user['role']==='admin'): echo 'selected'; endif; ?> >Admin</option>
				</select>
			</div>

			<button type="submit" name="update" class="btn btn-primary">Update</button>
			<p></p>


		<a href="logout.php" class="btn btn-danger">Log out</a>
		</form>




<?php require_once 'layouts/footer.php'; 


?>