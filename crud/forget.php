<?php
require_once 'config.php';

if(isset($_SESSION['id'], $_SESSION['email'])){
	header('Location: dashboard.php');
	exit();
}


$message = $_SESSION['message']?? null; 

require_once 'layouts/header.php';
?>
		<form action="forgetemail.php" method="post">
			<?php require_once 'layouts/notification.php'; ?>
			<div class="form-group">
				<label for="exampleInputEmail1">Email address</label>
				<input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
				<small id="emailHelp" class="form-text text-muted">We will never share your email with anyone else.</small>
			</div>

			<button type="submit" name="forget" class="btn btn-primary">Reset Password</button>
			
			
			<p></p>
			<p>
			<a href="login.php" class="btn btn-primary">Login</a>
			</p>
		</form>
	
	<?php require_once 'layouts/footer.php'; ?>