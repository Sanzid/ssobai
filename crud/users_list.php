<?php
	
	require_once 'config.php';

	$query = 'SELECT id, username, email, active FROM users';
	$stmt = $connection-> query($query);

	$users = $stmt->fetchAll();
?>			
			<style>
				table thead{
					text-weight:bold;
				}
			</style>

			<table border="1" cellpadding="15">
				<thead>
					<tr>
						<th>ID</th>
						<th>Username</th>
						<th>Email</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($users as $user): ?>
						
					<tr>
						<td><?php echo $user['id']; ?></td>
						<td><?php echo $user['username']; ?></td>
						<td><?php echo $user['email']; ?></td>
						<td><?php echo (int)$user['active']=== 1 ? 'Active' : 'Inactive'; ?></td>
						
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>