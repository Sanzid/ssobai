<?php

require_once 'config.php';

if(!isset($_SESSION['id'], $_SESSION['email'])){
	$_SESSION['message'] = 'You need to login to access this page.' ;
	header('Location: login.php');
	exit();
}

if($_SESSION['role'] !== 'admin'){
	$_SESSION['message'] = 'You need to be an admin to access this page.' ;
	header('Location: dashboard.php');
	exit();
}

$query= "SELECT id, username, email, active FROM users WHERE role='user'";
$stmt= $connection->prepare($query);
$stmt->execute();

$users = $stmt->fetchAll();

$message = $_SESSION['message'] ?? null;

require_once 'layouts/header.php';
?>

		<div class="alert alert-info">
			You Have been Logged In as, <?php echo $_SESSION['email']; ?>
			(<?php echo $_SESSION['role']; ?>)
		</div>

		<div>
			<p>
				<a href="create_user.php" class="btn btn-success btn-block">
					Create User
				</a>
			</p>

			<?php require_once 'layouts/notification.php' ?>

			<p>
				<a href="generate_pdf.php" class="btn btn-sm btn-info">PDF</a>
			</p>

			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>ID</th>
						<th>Username</th>
						<th>Email</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($users as $user): ?>
					<tr>
						<td><?php echo $user['id']; ?></td>
						<td><?php echo $user['username']; ?></td>
						<td><?php echo $user['email']; ?></td>
						<td><?php echo (int)$user['active']=== 1 ? 'Active' : 'Inactive'; ?></td>
						<td>
							<a href="edit_user.php?id=<?php echo $user['id']; ?>" class="btn btn-sm btn-info">Edit</a>
							<form action="delete_user.php" method="post">
								<input type="hidden" name="user_id" value=<?php echo $user['id']; ?>>
								<button type="submit" class="btn btn-sm btn-danger" name="delete" 
										onclick="return confirm('Are you sure to delete?')">Delete
								</button>
							</form> 
				</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>

		<a href="logout.php" class="btn btn-danger">Log out</a>

	<?php require_once 'layouts/footer.php'; ?>