<?php
require_once 'config.php';

if(!isset($_SESSION['id'], $_SESSION['email'])){
	$_SESSION['message'] = 'You need to login to access Dashboard page.' ;
	header('Location: login.php');
	exit();
}

// $lang = 'bn';

$en=[
	'edit_profile' => 'Edit Profile',
	'change_password' => 'Change Password',
	'users' => 'Users',
];

$bn=[
	'edit_profile' => 'profile sompadon',
	'change_password' => 'chabi poriborton',
	'users' => 'beboharkari',
];
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>User Dashboar</title>
	<link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
</head>
<body>
	<div class="container-fluid">
		<div class="alert alert-info">
			You Have been Logged In as, <?php echo $_SESSION['email']; ?>
			(<?php echo $_SESSION['role']; ?>)

			
		</div>

		<div>
			<p>
				<a href="edit_profile.php">
					<?php echo trans('edit_profile'); ?>
				</a>
			</p>
			<p>
				<a href="change_password.php">
					<?php echo trans('change_password'); ?>
				</a>
			</p>

			<?php if($_SESSION['role']=='admin'): ?>
			<p>
				<a href="users.php">
					<?php echo trans('users'); ?>
				</a>
			</p>
			<?php endif; ?>
		</div>

		<a href="logout.php" class="btn btn-danger">
			<?php echo trans('logout'); ?>
		</a>

	<?php require_once 'layouts/footer.php';