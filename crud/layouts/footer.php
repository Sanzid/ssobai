<html>
<body>
	<div>
		<footer>
			<p class="text-center text-muted">
				Copyright &copy; Sanzid Hasan <?php echo date('Y'); ?>
			</p>

			<p class="text-center">
				<a href="?language=en">EN</a>
				<a href="?language=bn">BN</a>
				<a href="?language=jp">JP</a>

			</p>
		</footer>
	</div>
</body>
</html>