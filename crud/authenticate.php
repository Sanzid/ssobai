<?php

require_once 'config.php';

if(isset($_POST['login'])){
	$email= strtolower(trim($_POST['email']));
	$password= strtolower(trim($_POST['password']));


	$query = "SELECT id, password, active, role  FROM users WHERE email=:email" ;
	$stmt = $connection -> prepare($query);

	$stmt -> bindParam(':email', $email);
	$stmt -> execute();

	$user = $stmt->fetch();


	if($user){
		if(password_verify($password, $user['password'])=== true){
			if((bool)$user['active'] === false){
				notification('Please Activate your Account.', 'danger');
				redirect('login.php');
				exit();
			}

			$_SESSION['id']= $user['id'];
			$_SESSION['email']= $email;
			$_SESSION['role']= $user['role'];

			redirect('dashboard.php');
			echo 'Logged In Successfully!';
			die();
		}

			notification('Invalid Credentials!', 'danger');
			redirect('login.php');
			exit();
	}

	notification('User Not Found!', 'danger');
	redirect('login.php');
	exit();

}

?>