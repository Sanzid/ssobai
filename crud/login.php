<?php
require_once 'config.php';

if(isset($_SESSION['id'], $_SESSION['email'])){
	header('Location: dashboard.php');
	exit();
}


$message = $_SESSION['message']?? null; 

require_once 'layouts/header.php';
?>

		<form action="authenticate.php" method="post">
			<?php require_once 'layouts/notification.php'; ?>
			<div class="form-group">
				<label for="exampleInputEmail1">Email address</label>
				<input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
				<small id="emailHelp" class="form-text text-muted">We will never share your email with anyone else.</small>
			</div>
			
			<div class="form-group">
				<label for="exampleInputPassword1">Password</label>
				<input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password" required>
			</div>

			<button type="submit" name="login" class="btn btn-primary">Login</button>
			<p></p>
			<a href="index.php" class="btn btn-info ">Register</a>

			<a href="forget.php">Forgot Password?</a>
		</form>
	<?php require_once 'layouts/footer.php';

