<?php
require_once 'config.php';

if(isset($_POST['forget'])){
	$email= strtolower(trim($_POST['email']));

	$query='SELECT COUNT(id) as count FROM users WHERE email=:email';
	$stmt= $connection->prepare($query);

	$stmt->bindParam(':email', $email);
	$stmt->execute();

	$result = $stmt->fetch();
	$email_exist = $result['count'];

	if((bool)$email_exist===true){

		$token= sha1(md5($email.time().uniqid('', true)));

		$query = 'INSERT INTO password_resets (email, token) VALUES (:email,:token)';
		$stmt = $connection->prepare($query);

		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':token', $token);
		$stmt->execute();


		$mail = new \PHPMailer\PHPMailer\PHPMailer();

		try {
		    //Server settings
		    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
		    $mail->isSMTP();                                      // Set mailer to use SMTP
		    $mail->Host = 'smtp.mailtrap.io';  						// Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = 'a725effc96ed6a';                 // SMTP username
		    $mail->Password = '9f4de1af7a8b3a';                           // SMTP password
		    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 465;                                    // TCP port to connect to

		    //Recipients
		    $mail->setFrom('hello@dhaka.com', 'Sanzid');
		    $mail->addAddress($email);     // Add a recipient

		    //Content
		    $mail->isHTML();
		    $mail->Subject = 'Password Reset';
		    $mail->Body    = 'Dear, '. $email .'<br/>';
		    $mail->Body    .= 'Please Click this link to reset your password: <br/>';
		    $mail->Body    .= '<a href="http://localhost/ssb98/crud/reset.php?token='.$token.'">
		    http://localhost/ssb98/crud/reset.php?token='.$token.'</a>';
		    $mail->Body    .= '<br/>';
		    $mail->send();

		} catch (Exception $e) {
			echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;

		}

		$_SESSION['message']='Please Check Your Email. ';
		header('Location: login.php');
		exit();

}

}

?>