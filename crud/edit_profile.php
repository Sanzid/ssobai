<?php
require_once 'config.php';

if(!isset($_SESSION['id'], $_SESSION['email'])){
	$_SESSION['message'] = 'You need to login to access Dashboard page.' ;
	header('Location: login.php');
	exit();
}

$message = $_SESSION['message']?? null; 
$id = (int)$_SESSION['id'];
$query ='SELECT profile_photo,address FROM users WHERE id=:id';
$stmt= $connection->prepare($query);
$stmt->bindParam(':id', $id);
$stmt->execute();

$user= $stmt->fetch();

require_once 'layouts/header.php';
?>
		<div class="alert alert-info">
			You Have been Logged In as, <?php echo $_SESSION['email']; ?>
			(<?php echo $_SESSION['role']; ?>) 
		</div>

		<div>
			
			<form action="update_profile.php" method="post" enctype="multipart/form-data">
				<?php require_once 'layouts/notification.php'; ?>
				<div class="form-group">
					<label for="profile_photo"> Profile Photo: </label>
					<input type="file" name="profile_photo" class="form-control">
					<?php if(!empty($user['profile_photo'])): ?>
						<img src="uploads/<?php echo $user['profile_photo']; ?>" alt="Profile Photo" width="150">
					<?php endif; ?>
				</div>

				<div class="form-group">
					<label for="address"> Address: </label>
					<textarea name="address" class="form-control"><?php echo $user['address']; ?></textarea>
				</div>

				<button type="submit" name="update" class="btn btn-success">Update</button>
			</form>
		</div>

		<p></p>
		<a href="logout.php" class="btn btn-danger">Log out</a>
	
<?php require_once 'layouts/footer.php';