<?php

require_once 'config.php';

if(!is_logged_in()){
	notification('You are not logged in.', 'danger');
	redirect('login.php');
}

if(!is_admin()){
	notification('You need to be an admin to access this page', 'danger');
	redirect('login.php');
}

$message = $_SESSION['message'] ?? null;

require_once 'layouts/header.php';

?>
<div class="container">
		<form action="add_user.php" method="post">
			
			<?php require_once 'layouts/notification.php' ?>

			<div class="form-group">
				<label for="exampleInputEmail1">Email address</label>
				<input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
				<small id="emailHelp" class="form-text text-muted">We will never share your email with anyone else.</small>
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1">User Name</label>
				<input type="text" class="form-control" name="username" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter User Name" required>
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">Password</label>
				<input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password" required>
			</div>

			<button type="submit" name="register" class="btn btn-primary">Create User</button>
			<p></p>

			<a href="logout.php" class="btn btn-danger">Log out</a>
			
		</form>

		
	</div>


<?php require_once 'layouts/footer.php'; ?>