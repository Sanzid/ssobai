<?php 

require_once '../pdo/connection.php';

$query = 'SELECT * FROM users';
$stmt = $connection->prepare($query);
$stmt->execute();

$data= $stmt->fetchAll();

SELECT * FROM users;

?>

<table border="1" cellpadding="10">
	<thead>
		<tr>
			<th>ID</th>
			<th>Email</th>
			<th>User Name</th>
			<th>Password</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($data as $row): ?>
		<tr>
			<td><?php echo $row['id']; ?></td>
			<td><?php echo $row['email']; ?></td>
			<td><?php echo $row['username']; ?></td>
			<td><?php echo $row['password']; ?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
