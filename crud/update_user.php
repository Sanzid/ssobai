<?php

require_once 'config.php';

if(!is_logged_in()){

	notification('You need to login to access this page. ', 'danger');
	redirect('login.php');
}

if(!is_admin()){

	notification('You need to be an admin to access this page. ', 'danger');
	redirect('login.php');
}

$user_id = (int)$_POST['user_id'];

$query = 'SELECT count(id) as is_available FROM users WHERE id = :id';
$stmt = $connection->prepare($query);
$stmt->bindParam(':id', $user_id, PDO::PARAM_INT);
$stmt->execute();

$user = $stmt->fetch();


if((int)$user['is_available']=== 0){
	notification('User with id '.$user_id. ' does not exist', 'danger');
	redirect('users.php');
}


if (isset($_POST['update'])) {
	$username = strtolower(trim($_POST['username']));
	$email = strtolower(trim($_POST['email']));
	$address = trim($_POST['address']);
	$active = trim($_POST['active']);
	$role = trim($_POST['role']);
	$password = trim($_POST['password']);

	if(!empty($password)){
		$password = password_hash($password, PASSWORD_BCRYPT);
		$query = 'UPDATE users SET password=:password WHERE id=:id';
		$stmt = $connection->prepare($query);
		$stmt->bindParam(':password', $password);
		$stmt->bindParam(':id', $user_id, PDO::PARAM_INT);
		$stmt->execute();
	}

	$query = 'UPDATE users SET username=:username, email=:email, address=:address, active=:active, role=:role WHERE id=:id';
	$stmt = $connection->prepare($query);
	$stmt->bindParam(':username', $username);
	$stmt->bindParam(':email', $email);
	$stmt->bindParam(':address', $address);
	$stmt->bindParam(':active', $active);
	$stmt->bindParam(':role', $role);
	$stmt->bindParam(':id', $user_id);
	$stmt->execute();

	notification('User updated successfully!');
	redirect('users.php');
}