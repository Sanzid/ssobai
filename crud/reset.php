<?php
require_once 'config.php';

if(isset($_SESSION['id'], $_SESSION['email'])){
	header('Location: dashboard.php');
	exit();
}

$token=trim($_GET['token']);
$query = 'SELECT email FROM password_resets WHERE token=:token';
$stmt = $connection->prepare($query);
$stmt->bindParam(':token', $token);
$stmt->execute();

$user= $stmt->fetch();

if($user=== false){
	$_SESSION['message']='Invalid Token!';
	header('Location: login.php');
	exit();
}


$message = $_SESSION['message']?? null; 

require_once 'layouts/header.php';
?>

		<form action="resetpassword.php" method="post">
			<?php require_once 'layouts/notification.php'; ?>
			<div class="form-group">
				<label for="exampleInputEmail1">Email address</label>
				<input type="email" class="form-control" name="email" value="<?php echo $user['email']; ?>" required readonly>
				<small id="emailHelp" class="form-text text-muted">We will never share your email with anyone else.</small>
			</div>
			
			<div class="form-group">
				<label for="exampleInputPassword1">Password</label>
				<input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password" required>
			</div>

			<button type="submit" name="resetpassword" class="btn btn-primary">Set Password</button>
			<p></p>
		</form>
	
<?php require_once 'layouts/footer.php';


