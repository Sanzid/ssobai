<?php

require_once 'config.php';

if(!is_logged_in()){
	notification('You are not logged in.', 'danger');
	redirect('login.php');
}

if(!is_admin()){
	notification('You need to be an admin to access this page', 'danger');
	redirect('login.php');
}

$user_id = (int)$_POST['user_id'];

if(isset($_POST['delete'])){

	if($user_id === user()['id']){
		notification('You can not delete yourself.', 'danger');
		redirect('users.php');
	}

	$query = 'DELETE FROM users WHERE id=:id';
	$stmt = $connection->prepare($query);
	$stmt->bindParam(':id', $user_id);
	$stmt->execute();

	notification('User Deleted...');
	redirect('users.php');	
}
redirect('users.php');