<?php

if (!function_exists('db_connect')) {
	
	function db_connect(){

		$connection=false;

		$dsn = 'mysql:dbname=ssb98;host=localhost';

		try{
			$connection= new PDO($dsn,'root','');
			$connection ->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

		}catch(PDOException $e){
			
		}
		return $connection;
	}

}

if(!function_exists('notification')){

	function notification($message, $type='success'){
		$_SESSION['message'] = $message;
		$_SESSION['type'] = $type;
	}
}

if(!function_exists('redirect')){

	function redirect($location ='index.php'){
		header('Location:'.$location);
		exit();
	}
}

if(!function_exists('is_logged_in')){
	function is_logged_in(){

		return isset($_SESSION['id'], $_SESSION['email'], $_SESSION['role']);
	}
}

if(!function_exists('is_admin')){
	function is_admin(){
		return $_SESSION['role'] === 'admin';
	}
}

if(!function_exists('user')){
	function user(){
		if(!is_logged_in()){
			return false;
		}

		return [
			'id' => (int)$_SESSION['id'],
			'email' => $_SESSION['email'],
			'role' => $_SESSION['role'],
		];
	}
}

if(!function_exists('dd')){
	function dd($var){
		echo '<pre>';
		var_dump($var);
		echo '</pre>';
		die();
	}
}

if(!function_exists('trans')){
	function trans($key){
		$language = $_SESSION['language'] ?? 'en';
		$translated_string = json_decode(file_get_contents('languages/'. $language .'/dashboard.json'), true);

		return $translated_string[$key];
	}
}


?>