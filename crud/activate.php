<?php

require_once 'config.php';

$token = trim($_GET['token']);

$query = 'SELECT COUNT(id) AS count FROM users WHERE activation_token=:token';
$stmt = $connection->prepare($query);

$stmt -> bindParam('token', $token);
$stmt ->execute();

$result= $stmt->fetch();
$user_exist=$result['count'];


if((bool)$user_exist==ture){
	$query='UPDATE users SET active=1, activation_token=null WHERE activation_token=:token';
	$stmt = $connection->prepare($query);

	$stmt -> bindParam('token', $token);
	$stmt ->execute();

	$_SESSION['message'] = 'Account Activated! You can login now.';
	header('Location: login.php');
	exit();

}

$_SESSION['message'] = 'Invalid Token!';
header('Location: index.php');
exit();

?>